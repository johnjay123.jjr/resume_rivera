<!DOCTYPE HTML>
<html lang="en-US">
<head>
		<img src="images\ggwp.jpg" style="max-width:100%; height:auto;">
		

	<meta charset="UTF-8">
	<title></title>
	
	<link rel="stylesheet" href="css\style.css" />
</head>
<body>
	<div class="container">
	<header>
		<h1 id="name">Rivera, John Jay A.</h1> 
		
		<div id="designation">
		
			<span class="title">144 Pluto Street G.S.I.S. Heights, Matina Davao City<br /></span>
			<span class="organization">09501682044</span>
		
		</div>
		
		<div class="contact">
			<div class="email">johnjay123.jjr@gmail.com</div>
		</div>
		
	</header>
	
	<div class="content" role=main>
	
		<section id="objective">
			<h2 class="title">Career Objective</h2>
			<div class="description">
				<p>
					to obtain any position that will enable myself to use my strong organizational skills, Educational background and ability to work well with people, which will allow me to grow personally and professionally. </p>			
			</div> 
		</section>
		
		
		<section id="Education">
			<h2 class="title">Education</h2>
			
				
				<table>
					<thead>
						<td>Level</td>
						<td>Name of School</td>
						<td>School Graduated</td>
						
					</thead>
					
					
					<tr>
						<td>Tertiary</td>
						<td>University of Southeastern Philippines <br />
							Iñigo St. Bo. Obrero 8000 Davao City <br />
							B.S. Information Technoly
						</td>
						<td>2015-Present</td>
						
					</tr>
					
					<tr> 
						<td>Secondary</td>
						<td>Daniel R. Aguinaldo National High School</td>
						<td>2014-2015</td>
						
					</tr>					
					
				</table>
			
			
			</div> 
		</section>
			
		<section id="projects">
			<h2 class="title">Skills and Qualifications</h2>
			<div class="description">
				
			
				<section class="project">
					
					<div class="meta">
			
					</div>
					<p class="description"> <ul>
						<li>Ability to work with a team and work under pressure
						</li>
						<li>Good Communication skills</li>
						<li>Self-motivation</li>
						<li>Work Well independently</li>
						<li>Excellent time in management skills</li>
					</ul> . </p>
				</section> 
			
				<section class="project">
					<h2 class="title">Duties and Responsibilities</h2>
					<div class="meta">
						
					</div>
					<p class="description">
						<ul>
							<li>Adhered to the quality and work standards of the chain</li>
							<li>Maintained cleanliness in the work stations</li>
							<li>Assisted the other staffmembers when help is needed</li>
						</ul>
					
					</p>
				</section> 

				<section class="project">
					<h2 class="title">Affilation</h2>
					
					</div>
					<p class="description">
						Icean Pirates(2015-Present) <br />
						Bo, OberoUsep, Davao City  <br />
						Member

					</p>
						
	
</body>
</html>